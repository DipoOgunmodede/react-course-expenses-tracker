function ExpenseDate(props) {
    //month,day and year const seem like the kind of logic that would be generated in computed:{} in Vue. Not sure if there are built in methods objects in Vue so far

    //these three lines breaking off the date into 3 separate variables and localising them to strong. 
    const month = props.date.toLocaleString('en-GB', { month: 'long' });
    const day = props.date.toLocaleString('en-GB', { day: '2-digit' });
    const year = props.date.getFullYear();
    return (
        <div className="expense-date">
            <div className="expense-date__month">{month}</div>
            <div className="expense-date__year">{year}</div>
            <div className="expense-date__day">{day}</div>
        </div>
    )
}
export default ExpenseDate;